#ifndef IFTSIPMTOOL_H
#define IFTSIPMTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCb
#include "Event/MCFTDeposit.h"

/** @class IFTSiPMTool IFTSiPMTool.h
 *  
 *  Interface for the tool that adds noise from the SiPM and
 *  the efficiency function for detecting photons.
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

struct IFTSiPMTool : extend_interfaces<  IAlgTool > {

  // Return the interface ID
  DeclareInterfaceID ( IFTSiPMTool, 1, 0 );
  virtual void addNoise(LHCb::MCFTDeposits* depositConts) = 0;
  virtual int generateDirectXTalk(int nPhotons) = 0;
  virtual int generateDelayedXTalk(int nPhotons) = 0;
  virtual float generateDelayedXTalkTime() = 0;
  virtual float photonDetectionEfficiency(double wavelength) = 0;
  virtual bool sipmDetectsPhoton(double wavelength) = 0;

};
 
#endif // IFTSIPMTOOL_H
