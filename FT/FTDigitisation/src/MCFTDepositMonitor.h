#ifndef MCFTDEPOSITMONITOR_H 
#define MCFTDEPOSITMONITOR_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"

// from Event
#include "Event/MCHit.h"
#include "Event/MCFTDeposit.h"

// FTDet                                                                                                                                                      
#include "FTDet/DeFTDetector.h"

/** @class MCFTDepositMonitor MCFTDepositMonitor.h
 *
 *  @author Jeroen van Tilburg, Luca Pescatore
 *  @date   2017-03-23
 */
class MCFTDepositMonitor : public Gaudi::Functional::Consumer
                           <void(const LHCb::MCFTDeposits&),
                           Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg> > {

public: 
  
  MCFTDepositMonitor(const std::string& name,ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  void operator()(const LHCb::MCFTDeposits& deposits) const override;

private:

  void fillHistograms(const LHCb::MCFTDeposit* deposit, const std::string& hitType) const;

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
  
};
#endif // MCFTDEPOSITMONITOR_H
