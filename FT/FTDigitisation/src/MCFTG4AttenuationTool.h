#ifndef MCFTG4ATTENUATIONTOOL_H
#define MCFTG4ATTENUATIONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "IMCFTAttenuationTool.h"            // Interface

/** @class MCFTAttenuationTool MCFTAttenuationTool.h
 *
 *  Tool that reads attenutation maps from ConDB
 *
 *
 *  @author Martin Bieker based on implementation of M. Demmer and J. Wishahi
 *  @date   2015-01-15
 */


class MCFTG4AttenuationTool : public extends<GaudiTool,IMCFTAttenuationTool> {

public:
  using base_class::base_class;

  /// Initialize the transmission map
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  void attenuation(double x, double y, double& att, double& attRef) override;

private:
  bool validateMap();
  int findBin(const std::vector<double>& axis, double position);

  unsigned int m_nBinsX, m_nBinsY;
  std::vector<double> m_xEdges, m_yEdges, m_effDir, m_effRef;

  //properties
  Gaudi::Property<double> m_mirrorReflectivity{ this,
    "MirrorReflectivity", 0.75, // from LHCb-PUB-2014-020
    "Reflectivity of mirror at the end of the fibre mat (0-1)"};
 Gaudi::Property<bool> m_irradiatedFibres{ this,
    "IrradiatedFibres", true,
    "Simulate radiation damage (after 50fb^-1)"};
};
#endif // MCFG4TATTENUATIONTOOL_H
