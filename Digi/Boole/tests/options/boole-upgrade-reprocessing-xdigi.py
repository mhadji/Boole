from PRConfig import TestFileDB 
TestFileDB.test_file_db['upgrade-baseline-FT61-xdigi'].run()

# Use final attenuation map for the FT
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170301"
LHCbApp().CondDBtag = "sim-20171123-vc-md100"
