{% set used = [] -%}
{% macro section(labels) -%}
{% for mr in select_mrs(merge_requests, labels, used) %}
- {{mr.title}}, !{{mr.iid}} (@{{mr.author.username}}) {{find_tasks(mr)}}  
  {{mr.description|mdindent(2)}}
{% endfor %}
{%- endmacro %}

{{date}} {{project}} {{version}}
===

This version uses projects LHCb v43r1, Lbcom v21r1, Gaudi v29r0, LCG_91
 (Root 6.10.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*  

This version is a xxx release for xxx  

This version is released on `master` branch. The previous release on `master` branch  was Boole `v32r0`.  

### New features
{{ section(['new feature']) }}

### Enhancements
{{ section(['enhancement']) }}

### Bug fixes
{{ section(['bug fix']) }}

### Code modernisations and cleanups
{{ section(['cleanup', 'modernisation']) }}

### Monitoring changes
{{ section(['monitoring']) }}

### Changes to tests
{{ section(['testing']) }}

### Other
{{ section([[]]) }}
