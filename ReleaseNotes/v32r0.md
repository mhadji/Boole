2017-10-25 Boole v32r0
===
This version uses projects LHCb v43r1, Lbcom v21r1, Gaudi v29r0, LCG_91
 (Root 6.10.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*

This version is a development release for 2018 and upgrade simulations

This version is released on `master` branch. The previous release on `master` branch  was `Boole v31r3`.

## Change to release notes format
**As from this release, the file `BooleSys/doc/release.notes` is frozen**  
Instead, there will be a file per release in the new `ReleaseNotes` directory. e.g. this file is called `ReleaseNotes/v32r0.md`

## Changes to compiler support
**As from this release, support for gcc49 is dropped**  
This means that C++14 is now fully supported

**As from this release, support for gcc7 is added**  
Note that C++17 is not yet fully supported, waiting for support from ROOT  


## New or changed functionality
**[MR LHCb!793] Update default DDDB tags according to LHCBCNDB-646**  
Introduces new `ParticleTable.txt` in DDDB with following global tags:  
```
Name               Based on         Datatype  
dddb-20170721      dddb-20160318    2010  
dddb-20170721-1    dddb-20160318-1  2011  
dddb-20170721-2    dddb-20150928    2012,2013  
dddb-20170721-3    dddb-20150724    2015,2016,2017  
```
**[MR LHCb!777] New linker base class in Associators/AssociatorsBase**  
See LHCBPS-1742

**[MR !93, LHCb!867] Separate CaloAdcs and CaloDigits production on Demand**
Sets up separate decoders in DecoderDB for CaloDigits and CaloAdcs

**[MR !696] LHCBPS-1729: Record platform info in event header**  
- Added class LHCb::PlatformInfo to record build and run time platform and host information  
- Added platformInfo data member to ProcessHeader (changed version class version)  

**Multiple updates and fixes for Git CondDB - see LHCb v42r5, v42r6, v43r1 release notes**  



## Changes to upgrade simulation
**[MR !92] Add corrected interpolation of SiPM PDE to FTSiPMTool**
Closes LHCBSCIFI-85  

**[MR !84] Dev scifi xml attenuation tool** 
Closes LHCBSCIFI-37 and LHCBSCIFI-50  

**[MR !85, !87] Adapt SciFi digitisation to the new linkers** 

**[MR !80, !83] Scifi noise improvements**
Closes LHCBSCIFI-81, LHCBSCIFI-2, and LHCBSCIFI-82  

**[MR !78, !79] Dev scifi parametric wavelength**  
Closes LHCBSCIFI-52  

**[MR !64, LHCb!622] Scifi: New event model**  
The changes correspond to JIRA tasks: LHCBSCIFI-79, LHCBSCIFI-80  
The default simulation is switched to the detailed digitisation. This is about 2 times faster that the current improved digitisation.

## Code optimisations
**[MR Lbcom!132, LHCb!618] Rich MaPMT Support improvements**  

**[MR LHCb!713] Disable Boost pool allocators**  

**[MR !75, LHCb!643] Use GSL linear interpolator instead of cspline during initialisation in RichDet**  

## Bug fixes
**[MR !96, Lbcom!184, LHCb!909] Fix untested StatusCodes uncovered by gaudi/Gaudi!386**  

**[MR !76, LHCb!657] Printout bug fix in SimComponents**  

**[MR !74] Add missing include**  

**[MR !873] Fix an out-of-bounds read on a vector in DeRichHPDPanel**  


## Code modernisations and cleanups
**[MR !67, !70, LHCb!676] Move SciFi algorithms to Gaudi::Functional framework**  

**[MR !91, !77] Suppress compilation warnings from Boost headers**  

**[MR !71] Modernize MuonAlg**  

**[MR !86, LHCb!782] Modernize bankKiller, OdinTypesFilter, OdinBCIDFilter**  

**Multiple code modernisations, see LHCb v43r1, v43r0 release notes**  

## Monitoring changes
**[MR Lbcom!144] Reduce CaloCluster2MC verbosity**

**[MR !731] Print out used CondDB tags even if global output level is not INFO**  

## Changes to tests
**[MR !81, !82] Add Boole().DisableTiming slot and set it to true in default QMTest setting**  
**[MR !100] Update exclusion in boole-mc11-spillover.qmt, fixes test with gcc7**  
